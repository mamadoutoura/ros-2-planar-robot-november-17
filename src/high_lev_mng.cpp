#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "eigen3/Eigen/Dense"
#include "rclcpp/rclcpp.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
 
 // High level manager node
class HighLevMng : public rclcpp::Node
{
  public:
    HighLevMng():
      Node("high_lev_mng")
      {      
      }
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<HighLevMng>());  
  rclcpp::shutdown();
  return 0;
}